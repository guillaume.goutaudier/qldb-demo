# Objective
The objective of this demo is to play wiht the AWS QLDB driver for python.

There is an official tutorial here: https://docs.aws.amazon.com/qldb/latest/developerguide/getting-started.python.html

In this demo I wanted to work on a much simpler use case and use pipenv instead of pip. 

# Prerequisite
In this demo we will use pipenv to install all the Python dependencies.

# QLDB install 
From the AWS consolde, go to the Amazon QLDB Servire (under Databases).

Simply click on `Create ledger` and give it a name, e.g. "qldbtest".

# Install Boto 3
Boto is the AWS SDK for Python.
```
$ pipenv install boto3
$ pipenv install awscli
$ aws configure
```

You can double check that everything works fine by starting a python3 shell and running the following commands:
```
$ python3
>>> import boto3
>>> s3 = boto3.resource('s3')
>>> for bucket in s3.buckets.all():
...   print(bucket.name)
```

# Install Pyqldb
```
$ pipenv install pyqldb
$ pipenv lock --pre
$ pipenv install pyqldb
```

# Connect to the QLDB database and create data
```
$ python3
>>> from pyqldb.driver.pooled_qldb_driver import PooledQldbDriver 
>>> qldb_driver = PooledQldbDriver(ledger_name="qldbtest")
>>> qldb_session = qldb_driver.get_session() 
>>> qldb_session.execute_statement('CREATE TABLE testtable')
>>> qldb_session.execute_statement("""INSERT INTO testtable { 'Data' : 'Hello World!' }""")
>>> qldb_session.close()
```

# Query data from the table
```
$ python3
>>> from pyqldb.driver.pooled_qldb_driver import PooledQldbDriver
>>> from amazon.ion.simpleion import dumps, loads
>>> qldb_driver = PooledQldbDriver(ledger_name="qldbtest")
>>> qldb_session = qldb_driver.get_session()
>>> result = qldb_session.execute_statement("""SELECT * from testtable AS entry WHERE entry.Data = 'Hello World!'""")
>>> for row in result:
>>>   print(dumps(row, binary=False, indent='  ', omit_version_marker=True))
>>> qldb_session.close()
```

# View the immutable history of all modifications in the table
```
$ python3
>>> from pyqldb.driver.pooled_qldb_driver import PooledQldbDriver
>>> from amazon.ion.simpleion import dumps, loads
>>> qldb_driver = PooledQldbDriver(ledger_name="qldbtest")
>>> qldb_session = qldb_driver.get_session()
>>> result = qldb_session.execute_statement("""SELECT * FROM history(testtable)""")
>>> for row in result:
>>>   print(dumps(row, binary=False, indent='  ', omit_version_marker=True))
>>> qldb_session.close()
```



